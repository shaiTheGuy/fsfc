from nltk.stem import PorterStemmer

stemmer = PorterStemmer()

def removeStemWords(doc):
        words = doc.split()
        newDoc = ""
        for word in words:
                newDoc+=stemmer.stem(word) + " "

        return newDoc