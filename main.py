from fsfc.utils import apriori
from fsfc.text.FTC import FTC
from sklearn.feature_extraction.text import CountVectorizer
import numpy as np
from pymongo import MongoClient
from fsfc.utils.stemmingRemover import removeStemWords
from fsfc.utils.stopWordsRemover import removeStopWords

vectorizer = CountVectorizer()
mongo_client = MongoClient("mongodb://devuser:999developersecrets@ds225253.mlab.com:25253/dev")

def main():
    # corpus = [
    #  'is is is the first document.',
    #  'This is the second second document.',
    #  'And the third one.',
    #  'Is this the first document?',
    #  ]
    # analyze = vectorizer.build_analyzer()
    # tokenized_docs = vectorize_dataset(corpus)
    # frequent_terms = list(apriori(vectorize_dataset(corpus), 0.25))

    monster_jobs = get_monster_jobs()
    frequent_terms = list(apriori(vectorize_dataset(monster_jobs), 0.5))
    # indeed_jobs = get_indeed_jobs()
    # indeed_cd = run_FTC(0.75, indeed_jobs)
    print(frequent_terms)

def vectorize_dataset(dataset):
    vectorizer = CountVectorizer()
    analyze = vectorizer.build_analyzer()
    tokenized_docs = []
    for document in dataset:
        tokenized_docs.append(analyze(document))
    return tokenized_docs

def run_FTC(minsup, dataset):
    return FTC(minsup).fit(vectorizer.fit_transform(dataset))

def get_indeed_jobs():
    indeed_jobs_collection = mongo_client.dev['indeed-jobs'].find({}, {"summary":1, "_id":0})
    results = []
    for document in indeed_jobs_collection:
        if(document):
            results.append(document.get("summary"))
    return results

def get_monster_jobs():
    monster_jobs_collection = mongo_client.dev['monster-jobs'].find({}, {"summary":1, "_id":0})
    results = []
    for document in monster_jobs_collection:
        if(document):
            results.append(document.get("summary"))
    return results
    
main()